require 'test_helper'

class SamplePersonMapsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @sample_person_map = sample_person_maps(:one)
  end

  test "should get index" do
    get sample_person_maps_url
    assert_response :success
  end

  test "should get new" do
    get new_sample_person_map_url
    assert_response :success
  end

  test "should create sample_person_map" do
    assert_difference('SamplePersonMap.count') do
      post sample_person_maps_url, params: { sample_person_map: { personTestedDuid: @sample_person_map.personTestedDuid, sampleBarCode: @sample_person_map.sampleBarCode, whoDidSampleScanNetid: @sample_person_map.whoDidSampleScanNetid } }
    end

    assert_redirected_to sample_person_map_url(SamplePersonMap.last)
  end

  test "should show sample_person_map" do
    get sample_person_map_url(@sample_person_map)
    assert_response :success
  end

  test "should get edit" do
    get edit_sample_person_map_url(@sample_person_map)
    assert_response :success
  end

  test "should update sample_person_map" do
    patch sample_person_map_url(@sample_person_map), params: { sample_person_map: { personTestedDuid: @sample_person_map.personTestedDuid, sampleBarCode: @sample_person_map.sampleBarCode, whoDidSampleScanNetid: @sample_person_map.whoDidSampleScanNetid } }
    assert_redirected_to sample_person_map_url(@sample_person_map)
  end

  test "should destroy sample_person_map" do
    assert_difference('SamplePersonMap.count', -1) do
      delete sample_person_map_url(@sample_person_map)
    end

    assert_redirected_to sample_person_maps_url
  end
end
