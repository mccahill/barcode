require "application_system_test_case"

class SamplePersonMapsTest < ApplicationSystemTestCase
  setup do
    @sample_person_map = sample_person_maps(:one)
  end

  test "visiting the index" do
    visit sample_person_maps_url
    assert_selector "h1", text: "Sample Person Maps"
  end

  test "creating a Sample person map" do
    visit sample_person_maps_url
    click_on "New Sample Person Map"

    fill_in "Persontestedduid", with: @sample_person_map.personTestedDuid
    fill_in "Samplebarcode", with: @sample_person_map.sampleBarCode
    fill_in "Whodidsamplescannetid", with: @sample_person_map.whoDidSampleScanNetid
    click_on "Create Sample person map"

    assert_text "Sample person map was successfully created"
    click_on "Back"
  end

  test "updating a Sample person map" do
    visit sample_person_maps_url
    click_on "Edit", match: :first

    fill_in "Persontestedduid", with: @sample_person_map.personTestedDuid
    fill_in "Samplebarcode", with: @sample_person_map.sampleBarCode
    fill_in "Whodidsamplescannetid", with: @sample_person_map.whoDidSampleScanNetid
    click_on "Update Sample person map"

    assert_text "Sample person map was successfully updated"
    click_on "Back"
  end

  test "destroying a Sample person map" do
    visit sample_person_maps_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Sample person map was successfully destroyed"
  end
end
