FROM ruby:2.4
RUN apt-get update -qq && apt-get install -y \
  build-essential \
  libpq-dev \
  nodejs \
  vim-tiny
RUN apt-get dist-upgrade -y
RUN apt-get autoremove -y
RUN mkdir /barcode
WORKDIR /barcode
COPY Gemfile /barcode/Gemfile
COPY Gemfile.lock /barcode/Gemfile.lock
RUN bundle install
CMD ["rails", "server"]
