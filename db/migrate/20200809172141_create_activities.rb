class CreateActivities < ActiveRecord::Migration[5.2]
  def change
    create_table :activities do |t|
      t.string :action
      t.string :note
      t.string :netid
      t.string :duid

      t.timestamps
    end
  end
end
