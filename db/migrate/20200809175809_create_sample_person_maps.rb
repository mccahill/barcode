class CreateSamplePersonMaps < ActiveRecord::Migration[5.2]
  def change
    create_table :sample_person_maps do |t|
      t.string :sampleBarCode
      t.string :personTestedDuid
      t.string :whoDidSampleScanNetid

      t.timestamps
    end
  end
end
