class CreateUsers < ActiveRecord::Migration[5.2]
  def change
    create_table :users do |t|
      t.string :displayName
      t.string :netid
      t.string :duid
      t.string :email

      t.timestamps
    end
  end
end
