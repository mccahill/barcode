class SamplePersonMapsController < ApplicationController
  before_action :set_sample_person_map, only: [:show, :edit, :update, :destroy]

  # GET /sample_person_maps
  # GET /sample_person_maps.json
  def index
    @sample_person_maps = SamplePersonMap.all
  end

  # GET /sample_person_maps/1
  # GET /sample_person_maps/1.json
  def show
  end

  # GET /sample_person_maps/new
  def new
    @sample_person_map = SamplePersonMap.new
  end

  # GET /sample_person_maps/1/edit
  def edit
  end

  # POST /sample_person_maps
  # POST /sample_person_maps.json
  def create
    @sample_person_map = SamplePersonMap.new(sample_person_map_params)

    respond_to do |format|
      if @sample_person_map.save
        format.html { redirect_to @sample_person_map, notice: 'Sample person map was successfully created.' }
        format.json { render :show, status: :created, location: @sample_person_map }
      else
        format.html { render :new }
        format.json { render json: @sample_person_map.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /sample_person_maps/1
  # PATCH/PUT /sample_person_maps/1.json
  def update
    respond_to do |format|
      if @sample_person_map.update(sample_person_map_params)
        format.html { redirect_to @sample_person_map, notice: 'Sample person map was successfully updated.' }
        format.json { render :show, status: :ok, location: @sample_person_map }
      else
        format.html { render :edit }
        format.json { render json: @sample_person_map.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /sample_person_maps/1
  # DELETE /sample_person_maps/1.json
  def destroy
    @sample_person_map.destroy
    respond_to do |format|
      format.html { redirect_to sample_person_maps_url, notice: 'Sample person map was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_sample_person_map
      @sample_person_map = SamplePersonMap.find(params[:id])
    end

    # Only allow a list of trusted parameters through.
    def sample_person_map_params
      params.require(:sample_person_map).permit(:sampleBarCode, :personTestedDuid, :whoDidSampleScanNetid)
    end
end
