json.extract! sample_person_map, :id, :sampleBarCode, :personTestedDuid, :whoDidSampleScanNetid, :created_at, :updated_at
json.url sample_person_map_url(sample_person_map, format: :json)
