json.extract! admin, :id, :displayName, :netid, :duid, :created_at, :updated_at
json.url admin_url(admin, format: :json)
