json.extract! user, :id, :displayName, :netid, :duid, :email, :created_at, :updated_at
json.url user_url(user, format: :json)
