Rails.application.routes.draw do
  resources :sample_person_maps
  resources :users
  resources :admins
  resources :activities
  get 'home/index'
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
